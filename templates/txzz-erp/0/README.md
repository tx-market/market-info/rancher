# 曦点数据 网银数据采集 

### 说明:
 此模板创建、扩展一个多节点的网银数据采集系统集群（基于rancher环境）。通过配置生成适用于rancher环境的元数据.
 集群规模可在部署完成后根据需要横向扩容。
 
### 用法:

  从catalog中选择曦点数据 - 网银数据采集. 
 
 输入节点数量, 配置一下参数

 Change the following zookeeper default parameters, if you need:

- ZK_DATA_DIR="/opt/zk/data"
- ZK_INIT_LIMIT="10"
- ZK_MAX_CLIENT_CXNS="500"
- ZK_SYNC_LIMIT="5"
- ZK_TICK_TIME="2000"
- host_label=""                         # Host label where to deploy zookeeper.
 
 点击 创建.
 

 注意: 当你扩展集群或升级节点时, 系统将会先停止当前服务后完成扩展（升级），此过程服务不可用。
